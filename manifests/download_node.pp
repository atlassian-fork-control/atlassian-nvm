define nvm::download_node() {
  validate_string($name)

  exec { "install ${name} with nvm":
    command => "/bin/bash -l -c 'nvm install ${name}'",
    require => [File['/etc/profile.d/nvm.sh']],

  }
}
