class nvm::params {
  $version              = 'v0.30.1'
  $default_node_version = '5.4'
  $node_versions        = ['5.4']
  $install_dir          = '/usr/local/nvm/'
}
